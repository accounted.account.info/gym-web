import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './core/guard/auth.guard';
import { EntriesComponent } from './modules/entries/entries-component';
import { HomeComponent } from './modules/home/home-component';
import { PlansComponent } from './modules/plans/plans-component';
import { UsersComponent } from './modules/users/users-component';

export const UsersRoute = {
  path: 'users',
  component: UsersComponent,
  data: {
    name: 'Clientes y Personal'
  },
  canActivate : [AuthGuard]
}

export const PlansRoute = {
  path: 'plans',
  component: PlansComponent,
  data: {
    name: 'Registros de Entrada'
  },
  canActivate : [AuthGuard]
}

export const EntriesRoute = {
  path: 'entries',
  component: EntriesComponent,
  data: {
    name: 'Registros de Entrada',
    oldEntries: true
  },
  canActivate : [AuthGuard]
}

export const HomeRoute = {
  path: '',
  component: HomeComponent,
  data: {
    name: 'Stats'
  },
  canActivate : [AuthGuard]
}


const routes: Routes = [
  UsersRoute,
  EntriesRoute,
  PlansRoute,
  HomeRoute
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
