import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MatSliderModule } from '@angular/material/slider';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UsersModule } from './modules/users/users-module';
import { MatNativeDateModule, MAT_DATE_FORMATS } from '@angular/material/core';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { HeaderModule } from './modules/header/header-module';
import { StickyModule } from './modules/sticky/sticky-module';
import { EntriesModule } from './modules/entries/entries-module';
import { DATE_FORMATS } from './models/date-format';
import { HomeModule } from './modules/home/home-module';
import { PlansModule } from './modules/plans/plans-module';
import { LoadingModule } from './modules/loading/loading.module';

@NgModule({
  declarations: [
    AppComponent    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSliderModule,
    HttpClientModule,
    UsersModule,
    EntriesModule,
    MatNativeDateModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
    HeaderModule,
    StickyModule,
    HomeModule,
    PlansModule,
    LoadingModule
  ],
  providers: [
    {provide: MAT_DATE_FORMATS, useValue: DATE_FORMATS}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

