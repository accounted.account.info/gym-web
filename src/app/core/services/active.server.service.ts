import { HttpHeaders } from "@angular/common/http";
import { Injectable, NgZone } from "@angular/core";
import { SseClient } from "ngx-sse-client";
import { Observable } from "rxjs";
import { ServerService } from "src/app/core/services/server.service";


@Injectable({
    providedIn: 'root',
})
export class ActiveServerService {

    constructor( private _zone: NgZone,
                 private serverService: ServerService,
                 private sseClient: SseClient) {}

    getEntriesEvent(url: string): Observable<Event> {
        const headers = new HttpHeaders();
        return this.sseClient.stream(url, { keepAlive: true, reconnectionDelay: 1_000, responseType: 'event' }, { headers }, 'GET');
    } 
}