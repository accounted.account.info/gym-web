export interface Entries {
    id?: number;
    userid: number;
    name: string;
    lastname: string;
    planId?: string;
    planName?: string;
    planEndDate?: string;
    counting: number;
    checktime: string; //YYYY-MM-DD hh:mm:ss
    sesiones: number;
    tipo?: string;
}
