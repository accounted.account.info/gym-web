export interface Plan {
    id?: number;
    name: string;
    enabled: number;
}
