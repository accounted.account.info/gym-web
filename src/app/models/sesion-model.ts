import { SystemUser } from "./system-user";

export interface Session {
    token: string;
    user: SystemUser;
}