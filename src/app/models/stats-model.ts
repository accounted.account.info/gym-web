export interface Stats {
    usersCount: number;
    oldUsersCount: number;
    activeUsersCount: number;
    activeUsersPercent: number;
    entries: number;
    entriesPerUser: number;
}