export interface SystemUser {
    id: number;
    name: string;
    lastname: string;
    email: string;
    username: string;
    password?: string;
}