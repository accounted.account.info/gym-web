export interface User {
    id?: number;
    name: string;
    lastname: string;
    planId?: string;
    planName?: string;
    gender?: string;
    planStartDate: string; //YYYY-MM-DD hh:mm:ss
    planEndDate: string; //YYYY-MM-DD hh:mm:ss
    phone?: string;
    email?: string;
    sesiones?: number;
}