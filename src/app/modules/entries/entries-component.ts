import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { interval, mergeMap, Observable } from 'rxjs';
import { ActiveServerService } from 'src/app/core/services/active.server.service';
import { Entries } from 'src/app/models/entries-model';
import { EntriesRefreshService } from 'src/app/services/entries/entries.refresh.service';
import { EntriesService } from 'src/app/services/entries/entries.service';
import { DateUtils } from 'src/app/utils/date-utils';
  
export interface EntryStatus {
    valid: boolean;
    status: string;
    entry: Entries;
}

@Component({
  selector: 'entries-componnet',
  templateUrl: './entries-component.html',
  styleUrls: ['./entries-component.scss']
})
export class EntriesComponent implements OnInit  {

  ELEMENT_DATA: Entries[] = [];
  displayedColumns: string[] = ['tipo','checktime', 'name', 'lastname', 'planName', 'planEndDate', 'counting','actions'];
  dataSource = new MatTableDataSource(this.ELEMENT_DATA);  

  loading = true;

  private _lastId = 0;
  @ViewChild(MatPaginator)
    paginator!: MatPaginator;
  @ViewChild(MatSort)
    sort!: MatSort;

  @Input('oldEntries') oldEntries = true;

  @Output() newEntryEmitter = new EventEmitter<EntryStatus>();

  constructor( readonly entriesService: EntriesService,
               private _snackBar: MatSnackBar,
               readonly entriesRefreshService: EntriesRefreshService,
               readonly activeServerService: ActiveServerService) {}

  dateUtils = new DateUtils;

  ngOnInit(): void {
      if ( !this.oldEntries ) {
        //this.autorefresh();   
        this.refresh();
        this.setServerService();
      } 
      else this.oldRefresh();
  }

  setServerService() {
      this.entriesService.getEntriesServerService().subscribe( (newEvent) => {
        //console.log(`new event is ${newEvent}`);
        if( newEvent && !this.oldEntries ) {
          this.entriesService.getEntries().subscribe( (data) => {
            this.doRefresh(data);
          });
        }
      });  
  }

  autorefresh() {
    interval(1000)
    .subscribe ( () => {
        this.entriesService.getEntries().subscribe( (data) => {
            this.doRefresh(data);
        });
    }); 
  }

  doRefresh( data: Entries[], override: boolean = false ){
    if (data.length > 0 ) {      
        this.loading = true;     
        const localData = data[0];
        const newId = (localData.id) ? localData.id : 0 ;       
        if ( newId !== this._lastId || override ) {
            this.dataSource = new MatTableDataSource(data);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
            this._lastId = newId;
            this.entriesRefreshService.setNewEntry();
            if( !override ) this.newEntryEmitter.emit(
                this.getEntryStatus( localData, localData.checktime, (localData?.planEndDate) ? localData.planEndDate : '3000-01-01 00:00:00' )
            );
        }
        this.loading = false;
    }
  }

  refresh() {
    if ( !this.oldEntries )  this.entriesService.getEntries().subscribe( (data) => {
        this.doRefresh(data);
    }); 
    else this.entriesService.getOldEntries().subscribe( (data) => {
        this.doRefresh(data, true);
    }); 
  }

  oldRefresh() {
    this.entriesService.getOldEntries().subscribe( (data) => {
        this.doRefresh(data,true);
    });  
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  goodEntry(entry: Entries): boolean {
    try {
        return ( this.goodEntryComparison(entry.checktime, (entry.planEndDate) ? entry.planEndDate : '3000-01-01' ) 
                 && ( ( (entry.counting) ? entry.counting : 0 ) <= entry.sesiones ) );
    } catch (error) {
        return false;
    }    
  }

  goodEntryDates(stringDate: string): boolean {
    try {
        const newDate = new Date(stringDate);
        const today = new Date();
        return ( newDate < today ) ? false : true ;
    } catch (error) {
        return false;
    }    
  }

  goodEntryComparison(checkinDate: string, planEndDate: string): boolean {
    return this.dateUtils.goodEntryComparison(checkinDate,planEndDate);   
  }

  getEntryStatus(entry: Entries, checkinDate: string, planEndDate: string): EntryStatus {
    const goodDate = this.goodEntryComparison(checkinDate,planEndDate);
    const counters = (entry.counting <= entry.sesiones);
    const entryStatus: EntryStatus = {
        valid: ( goodDate && counters ),
        status: `${ ( goodDate && counters ) ? 'VALIDO':'' } ${ (!goodDate) ? '<VENCIDO>':'' } ${ (!counters) ? '<SESIONES EXCEDIDAS>':'' }`,
        entry: entry
    }
    return entryStatus;
  }
  
  dateOnly(stringDate: string): string {
    return this.dateUtils.shortDate(stringDate);
  }

  deleteOld(){
    //TODO: delete old entries
  }

  deleteEntry(id: number) {
    this.entriesService.deleteEntry(id).subscribe( (data)=>{
        let snackBarRef = this._snackBar.open(`Entrada borrada`, 'OK');
        this.doRefresh(data, true);
    });
  }
}