import { Component, OnInit } from '@angular/core';
import { Stats } from 'src/app/models/stats-model';
import { EntriesRefreshService } from 'src/app/services/entries/entries.refresh.service';
import { StatsService } from 'src/app/services/system/stats-service';

@Component({
  selector: 'home-componnet',
  templateUrl: './home-component.html',
  styleUrls: ['./home-component.scss']
})
export class HomeComponent implements OnInit {

  constructor( readonly statsService: StatsService,
               readonly entriesRefreshService: EntriesRefreshService ) {}

  stats: Stats | undefined = undefined ;
  breakpoint: any;
  rowRelation = '2:1'

  ngOnInit(): void {
      this.refresh();
      this.resize(window.innerWidth);
      this.setEntryService();
  }

  setEntryService() {
    this.entriesRefreshService.getState().subscribe( () => {   
      this.refresh(false);
    });
  }

  onResize(event: any) {
    this.resize(event.target.innerWidth);
  }

  resize(innerWidth: any) {
    if (innerWidth <= 400 ) {
      this.breakpoint = 1;
      this.rowRelation = '1:1.5';
    } else if (innerWidth <= 1000) {
      this.breakpoint = 1;
      this.rowRelation = '1:1';
    } else if (innerWidth <= 1500) {
      this.breakpoint = 2;
      this.rowRelation = '2:1';
    } else {
      this.breakpoint = 2;
      this.rowRelation = '2:0.7';
    }
  }

  getActiveUsersSpinnerValue(): number {
    return (this.stats?.activeUsersPercent) ? this.stats?.activeUsersPercent : 0
  }

  refresh(showLoading: boolean = true){
    if ( showLoading ) this.stats = undefined;
    this.statsService.getStats().subscribe( (stats: Stats) => {
      this.stats = stats;
    });
  }

  getWeekUsersSpinnerValue(): number {
    return Math.floor(100 * ((this.stats?.entriesPerUser)? this.stats?.entriesPerUser : 0 ) / ((this.stats?.activeUsersCount)? this.stats?.activeUsersCount : 1 ));
  }

  getWeekUsersText(): string {
    return `Esta semana han asistido ${this.stats?.entriesPerUser} clientes, con un total de ${this.stats?.entries} registros`;
  }

  getWeekUsersTextValue(): string {
    return `${this.stats?.entriesPerUser} clientes`;
  }
}