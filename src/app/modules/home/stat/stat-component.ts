import { Component, Input, OnInit } from '@angular/core';
import { ProgressSpinnerMode } from '@angular/material/progress-spinner';
import { Stats } from 'src/app/models/stats-model';
import { StatsService } from 'src/app/services/system/stats-service';

@Component({
  selector: 'stat-component',
  templateUrl: './stat-component.html',
  styleUrls: ['./stat-component.scss']
})
export class StatComponent implements OnInit {

  constructor( readonly statsService: StatsService ) {}

  stats: Stats | undefined = undefined ;

  @Input() spinnerValue: number | boolean = false;
  @Input() statValue: string | undefined = undefined;
  @Input() spinnerDiameter = 100;
  @Input() spinnerColor = 'primary';
  @Input() cardTitle: string | undefined = undefined; 
  @Input() cardSubtitle: string | undefined = undefined; 
  @Input() contentText: string | undefined = undefined;
  @Input() imgUrl: string | undefined = undefined;
  @Input() statIcon: string | undefined = undefined;

  ngOnInit(): void {
      this.refresh();
  }

  spinnerMode(mode: number | boolean ): ProgressSpinnerMode { 
    if (mode === true) return 'indeterminate';
    else return 'determinate';
  }

  getUrl(){
    return (this.imgUrl) ? this.imgUrl : '';
  }

  enableSpinner(mode: number | boolean): boolean {
    if (mode === true) return true;
    if (typeof mode == 'number' ) return true;
    return false;
  }

  getSpinnerValue(mode: number | boolean ): number {
    if (typeof mode == 'number' ) return mode;
    else return 0;
  }

  refresh(){
    this.stats = undefined;
    this.statsService.getStats().subscribe( (stats: Stats) => {
      this.stats = stats;
    });
  }
}