import { NgModule } from '@angular/core'; 
import { LoadingComponent } from './loading.component';
import { CommonModule } from '@angular/common';  
import { BrowserModule } from '@angular/platform-browser'; 
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';

@NgModule({
  declarations: [
    LoadingComponent
  ],
  imports: [
    CommonModule,
    BrowserModule, 
    MatProgressBarModule,    
    MatCardModule,
    MatFormFieldModule
  ],
  providers: [],
  exports: [
    LoadingComponent
  ]
})
export class LoadingModule { }