import {Component, EventEmitter, Inject, Input, OnInit, Output} from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { DateUtils } from 'src/app/utils/date-utils';
import { Plan } from 'src/app/models/plan-model';
import { PlansService } from 'src/app/services/plans/plans.service';

/**
 * @title Dialog with header, scrollable content and actions
 */
@Component({
  selector: 'new-plan-component',
  templateUrl: 'new-plan-component.html',  
  styleUrls: ['./new-plan-component.scss']
})
export class NewPlanComponent {

  @Output() addedPlanEvent = new EventEmitter<Plan>(); 

  constructor(public dialog: MatDialog) {}

  openDialog() {
    const dialogRef = this.dialog.open(NewPlanDialogComponent);

    dialogRef.afterClosed().subscribe( (result) => {
      if ( result !== false &&  result !== null ) {
          this.addedPlanEvent.emit(result);
      }
    });
  }
}

@Component({
  selector: 'new-plan-component-dialog',
  templateUrl: 'new-plan-component-dialog.html',  
  styleUrls: ['./new-plan-component.scss']
})
export class NewPlanDialogComponent implements OnInit {

    today: any;
    editMode= false;
    private _plan!: Plan;
    _id = 0;

    dateUtils = new DateUtils();

    @Input() set plan(plan: Plan) {
        this.editMode=true;
        this.fillPlan(plan);
        this._plan = plan;
    }
   
    get user(): Plan {
        return this.savePlan();
    }

    firstForm = new FormGroup({
        name: new FormControl(''),
        enabled: new FormControl(true)
    });
    
    constructor(@Inject(MAT_DIALOG_DATA) public data: Plan,
                readonly plansService: PlansService){
        const today = new Date();
        this.today =  today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    }

    ngOnInit(): void {    
        if (this.data?.id) {
            this.editMode = true;
            this.fillPlan(this.data);
        } 
    }

    savePlan(): Plan {

        if (this.editMode) {
            return {
                id: this._id,
                name: this.firstForm.get('name')?.value,
                enabled: this.firstForm.get('enabled')?.value
            }   
        } else return {
            name: this.firstForm.get('name')?.value,
            enabled: this.firstForm.get('enabled')?.value
        }     
    }

    fillPlan(plan: Plan){
        this._id = (plan.id) ? plan.id : 0;
        this.firstForm.get('name')?.setValue(plan.name);
        this.firstForm.get('enabled')?.setValue((plan.enabled > 0));
    }
}