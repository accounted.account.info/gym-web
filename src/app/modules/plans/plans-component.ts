import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Plan } from 'src/app/models/plan-model';
import { PlansService } from 'src/app/services/plans/plans.service';
import { DateUtils } from 'src/app/utils/date-utils';
import { NewPlanDialogComponent } from './new-plan/new-plan-component';


@Component({
  selector: 'plans-componnet',
  templateUrl: './plans-component.html',
  styleUrls: ['./plans-component.scss']
})
export class PlansComponent implements OnInit, AfterViewInit  {

  ELEMENT_DATA: Plan[] = [];
  displayedColumns: string[] = ['id', 'name', 'enabled', 'actions'];
  dataSource = new MatTableDataSource(this.ELEMENT_DATA);  
  panelOpenState = false;
  loading=true;
  @ViewChild(MatPaginator)
    paginator!: MatPaginator;
  @ViewChild(MatSort)
    sort!: MatSort;

  constructor( readonly PlansService: PlansService,
               private _snackBar: MatSnackBar,
               public dialog: MatDialog ) {}

  
  readonly dateUtils= new DateUtils;

  ngOnInit(): void {
      this.refresh();
  }

  refresh() {
    this.PlansService.getPlans().subscribe( (data) => {
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.loading=false;
    }); 
  }

  ngAfterViewInit() {
    //this.dataSource.paginator = this.paginator;
    //this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  newPlan( newPlan: Plan){
    this.PlansService.savePlan(newPlan).subscribe( (data) => {
        let snackBarRef = this._snackBar.open(`Persona: ${newPlan.name} Agregado`, 'OK');
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    });
  }

  editPlanConfirmation( newPlan: Plan){
    this.PlansService.editPlan(newPlan).subscribe( (data) => {
        let snackBarRef = this._snackBar.open(`Persona: ${newPlan.name} Editado`, 'OK');
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    });
  }

  switchPlanConfirmation( planToDelete: Plan){
    planToDelete.enabled = (planToDelete.enabled > 0) ? 0 : 1;
    this.PlansService.deletePlan( planToDelete ).subscribe( (data) => {
        let snackBarRef = this._snackBar.open(`Plan: ${planToDelete.name} ${(planToDelete.enabled) ? 'Activado' : 'Desactivado'}`, 'OK');
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    });
  }

  goodEntry(endDate: string){
    return this.dateUtils.goodEntry(endDate);
  }

  dateOnly(stringDate: string): string {
    return this.dateUtils.dateOnly(stringDate);
  }

  deletePlan(Plan: Plan) {
  }

  editPlan(Plan: Plan) {
    this.openPlanEditDialog(Plan);
  }

  openPlanEditDialog(Plan: Plan) {
    const dialogRef = this.dialog.open(NewPlanDialogComponent, { data: Plan, panelClass: 'blur-backdrop' });
    dialogRef.afterClosed().subscribe( (result) => {
      if ( result !== false &&  result !== null ) {
          this.editPlanConfirmation(result);
      }
    });
  }
  
}