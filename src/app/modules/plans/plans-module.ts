import { NgModule } from '@angular/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTableModule } from '@angular/material/table';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from '@angular/material/input';
import { NewPlanModule } from './new-plan/new-plan-module';
import { MatCardModule } from '@angular/material/card';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { RowActionsModule } from '../row-actions/row-actions-module';
import { MatExpansionModule } from '@angular/material/expansion';
import { PlansComponent } from './plans-component';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { LoadingModule } from '../loading/loading.module';

@NgModule({
  declarations: [
    PlansComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    NewPlanModule,
    MatCardModule,
    MatPaginatorModule,
    MatSortModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatSnackBarModule,
    RowActionsModule,
    MatExpansionModule,
    MatSlideToggleModule,
    LoadingModule
  ],
  providers: [],
  exports: [
    PlansComponent
  ]
})
export class PlansModule { }