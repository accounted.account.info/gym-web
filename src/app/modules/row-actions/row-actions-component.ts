import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
  
@Component({
  selector: 'row-action-componnet',
  templateUrl: './row-actions-component.html',
  styleUrls: ['./row-actions-component.scss']
})
export class RowActionsComponent {
  @Input('enableEdit') enableEdit = true;
  @Input('enableDelete') enableDelete= true;
  @Input('enableFinger') enableFinger= true;

  @Output() editEmitter = new EventEmitter<boolean>(true);
  @Output() deleteEmitter = new EventEmitter<boolean>(true);
  @Output() fingerEmitter = new EventEmitter<boolean>(true);
}