import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { EntryStatus } from '../entries/entries-component';
  
@Component({
  selector: 'sticky-component',
  templateUrl: './sticky-component.html',
  styleUrls: ['./sticky-component.scss']
})
export class StickyComponent implements OnInit {
    display= true;
    newReadings = true;
    private _play = false;

    constructor( private _snackBar: MatSnackBar ){}

    ngOnInit(): void {
        this._play = true;    
    }

    opened() {
        this.display= true;
        this.newReadings = false;
    }

    closed() {
        this.display= false;
    }

    checkNewEntry(entry: EntryStatus) {
        this._snackBar.open(`Registro: ${entry.entry.name} ${entry.entry.lastname} ${entry.status}`, 'OK');
        if(this._play) this.playAudio(entry.valid);
        if(!this.display) this.newReadings = true;
    }

    playAudio(valid: boolean ){
        let audio = new Audio();
        audio.src = (valid) ? "assets/sounds/correcto.mp3" : "assets/sounds/no_valido.mp3" ;
        audio.load();
        var isPlaying = audio.currentTime > 0 && !audio.paused && !audio.ended && audio.readyState > audio.HAVE_CURRENT_DATA;
        if (!isPlaying) {
            audio.play();
        }
    }

}