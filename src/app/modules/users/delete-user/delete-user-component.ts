import { Component,  Inject,  OnInit } from '@angular/core';
import { MAT_DIALOG_DATA} from '@angular/material/dialog';
import { User } from 'src/app/models/user-model';
import { UsersService } from 'src/app/services/users/users.service';
import { PlansService } from 'src/app/services/plans/plans.service';
import { DateUtils } from 'src/app/utils/date-utils';

@Component({
  selector: 'delete-user-component-dialog',
  templateUrl: 'delete-user-component-dialog.html',  
  styleUrls: ['./delete-user-component.scss']
})
export class DeleteDialogComponent implements OnInit {

    today: any;
    user!: User;

    dateUtils = new DateUtils();
    
    constructor(@Inject(MAT_DIALOG_DATA) public data: User,
                readonly usersService: UsersService,
                readonly plansService: PlansService ){
        const today = new Date();
        this.today =  today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    }

    ngOnInit(): void {
        this.user = this.data;
    }

    dateToString(aDate: Date): string {
        return this.dateUtils.dateToString(aDate);
    }

    deleteUser(): User {
        return this.user;
    }
}