import {Component, EventEmitter, Inject, Input, OnInit, Output} from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { User } from 'src/app/models/user-model';
import { UsersService } from 'src/app/services/users/users.service';
import { PlansService } from 'src/app/services/plans/plans.service';
import { Plan } from 'src/app/models/plan-model';
import { DateUtils } from 'src/app/utils/date-utils';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import { DATE_FORMATS } from 'src/app/models/date-format';
import * as _moment from 'moment';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter } from '@angular/material-moment-adapter';

const moment = _moment;

/**
 * @title Dialog with header, scrollable content and actions
 */
@Component({
  selector: 'new-user-component',
  templateUrl: 'new-user-component.html',  
  styleUrls: ['./new-user-component.scss']
})
export class NewUserComponent {

  @Output() addedUserEvent = new EventEmitter<User>(); 

  constructor(public dialog: MatDialog) {}

  openDialog() {
    const dialogRef = this.dialog.open(NewUserDialogComponent);

    dialogRef.afterClosed().subscribe( (result) => {
      if ( result !== false &&  result !== null ) {
          this.addedUserEvent.emit(result);
      }
    });
  }
}

@Component({
  selector: 'new-user-component-dialog',
  templateUrl: 'new-user-component-dialog.html',  
  styleUrls: ['./new-user-component.scss'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },

    {provide: MAT_DATE_FORMATS, useValue: DATE_FORMATS},
  ],
})
export class NewUserDialogComponent implements OnInit {

    today: any;
    plansList: Plan[] = [];
    editMode= false;
    _id = 0;

    dateUtils = new DateUtils();

    @Input() set user(user: User) {
        this.editMode=true;
        this.fillUser(user);
    }
   
    get user(): User {
        return this.saveUser();
    }

    firstForm = new FormGroup({
        name: new FormControl(''),
        lastname: new FormControl(''),  
        phone: new FormControl(''),
        email: new FormControl('', [Validators.required, Validators.email]),

    });

    secondForm = new FormGroup({
        planId: new FormControl(1),
        planStartDate: new FormControl(moment()),
        planEndDate: new FormControl(moment()),
        sesiones: new FormControl('3')
    });
    
    constructor(@Inject(MAT_DIALOG_DATA) public data: User,
                readonly usersService: UsersService,
                readonly plansService: PlansService){
        const today = new Date();
        this.today =  today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    }

    ngOnInit(): void {        
        this.getPlans();
        if (this.data?.id) {
            this.editMode = true;
            this.fillUser(this.data);
        } else {
            const todayDate = new Date();
            this.secondForm.get('planStartDate')?.setValue(todayDate);
            this.secondForm.get('planEndDate')?.setValue(this.getNextMonths(1));
        }
    }

    getNextMonths(months: number) {
        const today: Date = new Date();
        today.setMonth(today.getMonth()+ months);
        return new Date(today);
    }

    getPlans() {
        this.plansService.getPlans().subscribe( (data) => {
            this.plansList = data;
        });
    }

    getErrorMessage() {
        if (this.firstForm.get('email')?.hasError('required')) {
            return 'Debe ingresar un email';
        }        
        return this.firstForm.get('email')?.hasError('email') ? 'No es un email válido' : '';
    }

    dateToString(aDate: any): string {
        let dDate = new Date;
        let isMoment = (aDate instanceof moment);
        if (isMoment) dDate = aDate.toDate();
        else dDate = aDate;
        return this.dateUtils.dateToString(dDate);
    }

    saveUser(): User {

        if (this.editMode) {
            return {
                id: this._id,
                name: this.firstForm.get('name')?.value,
                lastname: this.firstForm.get('lastname')?.value,
                email: this.firstForm.get('email')?.value, 
                planId: this.secondForm.get('planId')?.value, 
                planEndDate: this.dateToString(this.secondForm.get('planEndDate')?.value),
                planStartDate: this.dateToString(this.secondForm.get('planStartDate')?.value),
                phone: this.firstForm.get('phone')?.value,
                sesiones: this.secondForm.get('sesiones')?.value
            }   
        } else return {
            name: this.firstForm.get('name')?.value,
            lastname: this.firstForm.get('lastname')?.value,
            email: this.firstForm.get('email')?.value, 
            planId: this.secondForm.get('planId')?.value, 
            planEndDate: this.dateToString(this.secondForm.get('planEndDate')?.value),
            planStartDate: this.dateToString(this.secondForm.get('planStartDate')?.value),
            phone: this.firstForm.get('phone')?.value,
            sesiones: this.secondForm.get('sesiones')?.value
        }     
    }

    fillUser(usr: User){
        this._id = (usr.id) ? usr.id : 0;
        this.firstForm.get('name')?.setValue(usr.name);
        this.firstForm.get('lastname')?.setValue(usr.lastname);
        this.firstForm.get('email')?.setValue(usr.email);
        this.secondForm.get('planId')?.setValue(usr.planId); 
        this.secondForm.get('planEndDate')?.setValue(new Date(usr.planEndDate));
        this.secondForm.get('planStartDate')?.setValue(new Date(usr.planStartDate));
        this.firstForm.get('phone')?.setValue(usr.phone); 
        this.secondForm.get('sesiones')?.setValue(usr.sesiones);
    }
}