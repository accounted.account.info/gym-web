import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { User } from 'src/app/models/user-model';
import { EntriesService } from 'src/app/services/entries/entries.service';
import { UsersService } from 'src/app/services/users/users.service';
import { DateUtils } from 'src/app/utils/date-utils';
import { DeleteDialogComponent } from './delete-user/delete-user-component';
import { NewUserDialogComponent } from './new-user/new-user-component';


@Component({
  selector: 'users-componnet',
  templateUrl: './users-component.html',
  styleUrls: ['./users-component.scss']
})
export class UsersComponent implements OnInit, AfterViewInit  {

  ELEMENT_DATA: User[] = [];
  displayedColumns: string[] = ['id', 'name', 'lastname', 'contact' , 'plan', 'planStartDate', 'planEndDate', 'actions'];
  dataSource = new MatTableDataSource(this.ELEMENT_DATA);  
  panelOpenState = false;
  @ViewChild(MatPaginator)
    paginator!: MatPaginator;
  @ViewChild(MatSort)
    sort!: MatSort;

  constructor( readonly usersService: UsersService,
               private _snackBar: MatSnackBar,
               public dialog: MatDialog,
               private entryService: EntriesService) {}

  
  readonly dateUtils= new DateUtils;

  ngOnInit(): void {
      this.refresh();
  }

  refresh() {
    this.usersService.getUsers().subscribe( (data) => {
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }); 
  }

  ngAfterViewInit() {
    //this.dataSource.paginator = this.paginator;
    //this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  newUser( newUser: User){
    if (newUser !== undefined ) this.usersService.saveUser(newUser).subscribe( (data) => {
        let snackBarRef = this._snackBar.open(`Persona: ${newUser.name} ${newUser.lastname} Agregada`, 'OK');
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    });
  }

  editUserConfirmation( newUser: User){
    if (newUser !== undefined ) this.usersService.editUser(newUser).subscribe( (data) => {
        let snackBarRef = this._snackBar.open(`Persona: ${newUser.name} ${newUser.lastname} Editada`, 'OK');
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    });
  }

  deleteUserConfirmation( userToDelete: User){
    this.usersService.deleteUser((userToDelete.id) ? userToDelete.id : 0 ).subscribe( (data) => {
        let snackBarRef = this._snackBar.open(`Persona: ${userToDelete.name} ${userToDelete.lastname} borrada`, 'OK');
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    });
  }

  goodEntry(endDate: string){
    return this.dateUtils.goodEntry(endDate);
  }

  dateOnly(stringDate: string): string {
    return this.dateUtils.dateOnly(stringDate);
  }

  deleteUser(user: User) {
    this.openUserDeleteDialog(user);
  }

  editUser(user: User) {
    this.openUserEditDialog(user);
  }

  manualUserEntry(id: number) {
    this.entryService.saveEntry(id).subscribe( (data) => {
        let snackBarRef = this._snackBar.open(`Entrada realizada`, 'OK');
    });
  }

  openUserEditDialog(user: User) {
    const dialogRef = this.dialog.open(NewUserDialogComponent, { data: user });
    dialogRef.afterClosed().subscribe( (result) => {
      if ( result !== false &&  result !== null ) {
          this.editUserConfirmation(result);
      }
    });
  }
  
  openUserDeleteDialog(user: User) {
    const dialogRef = this.dialog.open(DeleteDialogComponent, { data: user });
    dialogRef.afterClosed().subscribe( (result) => {
      if ( result !== false &&  result !== null ) {
          this.deleteUserConfirmation(result);
      }
    });
  }
}