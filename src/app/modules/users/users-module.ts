import { NgModule } from '@angular/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTableModule } from '@angular/material/table';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UsersComponent } from './users-component';
import { MatInputModule } from '@angular/material/input';
import { NewUserModule } from './new-user/new-user-module';
import { MatCardModule } from '@angular/material/card';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { RowActionsModule } from '../row-actions/row-actions-module';
import { DeleteUserModule } from './delete-user/new-user-module';
import { MatExpansionModule } from '@angular/material/expansion';

@NgModule({
  declarations: [
    UsersComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    NewUserModule,
    MatCardModule,
    MatPaginatorModule,
    MatSortModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatSnackBarModule,
    RowActionsModule,
    DeleteUserModule,
    MatExpansionModule
  ],
  providers: [],
  exports: [
    UsersComponent
  ]
})
export class UsersModule { }