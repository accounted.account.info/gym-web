import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EntriesRefreshService {

  newEntry$ = new BehaviorSubject<boolean>(false);

  setNewEntry() {
      this.newEntry$.next(true);
  }

  getState() {
    return this.newEntry$.asObservable();
  }

}
