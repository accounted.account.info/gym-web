import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../../models/user-model'
import { map, Observable, of } from 'rxjs';
import { Entries } from 'src/app/models/entries-model';
import { ActiveServerService } from 'src/app/core/services/active.server.service';


export interface EntryEvent {
  newEntry: boolean;
  error?: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class EntriesService {

  private endpoint = '/api/entries'; 
  private endpointOld = '/api/old-entries'; 
  private eventsEndpoint = '/api/entries/events/new'; 

  constructor(
    private http: HttpClient,
    private activeServerService: ActiveServerService) { }

  getEntries(): Observable<Entries[]> {
    return this.http.get<Entries[]>(this.endpoint);
  }
  
  getOldEntries(): Observable<Entries[]> {
    return this.http.get<Entries[]>(this.endpointOld);
  }

  saveEntry(userId: number): Observable<Entries[]> {
    return this.http.post<Entries[]>(this.endpoint, { userId: userId });
  }

  deleteEntry(id: number):  Observable<any> {
    return this.http.delete<any>( `${this.endpoint}/${id}` );
  }

  getEntriesServerService(): Observable<EntryEvent>{
    return this.activeServerService.getEntriesEvent(this.eventsEndpoint).pipe(
      map( (event) => {
        try {
          if (event.type === 'error') {
            return {
              newEntry: false,
              error: true
            };
          } else {
            const messageEvent = event as MessageEvent; 
            const data = JSON.parse(messageEvent.data);
            return (data.newEntry);
          }
        } catch (error) {
          console.info(error);
          return {
            newEntry: false,            
            error: true
          };
        }
      })
    );
  }

}
