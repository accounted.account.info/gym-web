import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Plan } from 'src/app/models/plan-model';

@Injectable({
  providedIn: 'root'
})
export class PlansService {

  private endpoint = '/api/departments'; 

  constructor(
    private http: HttpClient) { }

  getPlans(): Observable<Plan[]> {
    return this.http.get<Plan[]>(this.endpoint);
  }

  savePlan(plan: Plan): Observable<Plan[]> {
    return this.http.post<Plan[]>(this.endpoint, plan);
  }

  editPlan(plan: Plan): Observable<Plan[]> {
    return this.http.post<Plan[]>(this.endpoint, plan);
  }

  deletePlan(plan: Plan): Observable<Plan[]> {
    return this.http.put<Plan[]>(`${this.endpoint}/${plan.id}`, plan);
  }

}