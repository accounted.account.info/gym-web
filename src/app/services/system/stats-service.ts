import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Stats } from 'src/app/models/stats-model';

@Injectable({
  providedIn: 'root'
})
export class StatsService {

  private endpoint = '/api/stats'; 

  constructor(
    private http: HttpClient) { }

  getStats(): Observable<Stats> {
    return this.http.get<Stats>(this.endpoint);
  }
}