import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../../models/user-model'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private endpoint = '/api/users'; 

  constructor(
    private http: HttpClient) { }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.endpoint);
  }

  saveUser(user: User): Observable<User[]> {
    return this.http.post<User[]>(this.endpoint, user);
  }

  editUser(user: User): Observable<User[]> {
    return this.http.put<User[]>(this.endpoint, user);
  }

  deleteUser(id: number):  Observable<any> {
    return this.http.delete<any>( `${this.endpoint}/${id}` );
  }
}
