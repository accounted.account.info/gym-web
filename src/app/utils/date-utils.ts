
export class DateUtils {

    goodEntry(stringDate: string): boolean {
        try {
            const newDate = new Date(stringDate);
            const today = new Date();
            return ( newDate < today ) ? false : true ;
        } catch (error) {
            return false;
        }    
    }

    dateOnly(stringDate: string): string {
        const newDate= new Date(stringDate);
        return newDate.toLocaleDateString('es-ES', { weekday: 'narrow', year: 'numeric', month: 'long', day: 'numeric' });
    }

    shortDate(stringDate: string): string {
        try {
            const aDate = new Date(stringDate);
            return aDate.getFullYear()+'-'+(String(aDate.getMonth()+1).padStart(2, '0'))+'-'+String(aDate.getDate()).padStart(2, '0');
        } catch (error) {
            return '';
        }
    }

    
    dateToString(aDate: Date): string {
        try {
            return aDate.getFullYear()+'-'+(String(aDate.getMonth()+1).padStart(2, '0'))+'-'+String(aDate.getDate()).padStart(2, '0');
        } catch (error) {
            return '';
        }
    }

    goodEntryComparison(checkinDate: string, planEndDate: string): boolean {
        try {
            const newDate = new Date(checkinDate);
            const endDate = new Date(planEndDate);
            return ( newDate < endDate );
        } catch (error) {
            return false;
        }    
    }
}

